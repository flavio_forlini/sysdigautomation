import sys
import re
import json
import requests
import os
from sdcclient import IbmAuthHelper, SdMonitorClient, SdcClient
from sdcclient._common import _SdcCommon
import datetime;
 
# ct stores current time
def timestamp():
    ct = datetime.datetime.now()
    #print("current time:-", ct)
    return ct

#opening log file
logfile = open("spx_exp_imp_script.log", "a")
logfile.write(str(timestamp())+" INFO ####################### BEGIN #######################"+"\n")                                 
# Parse arguments
if len(sys.argv) < 5:
    print(('usage: %s <sysdig-token-src> <sysdig-token-dst> <namespace> <PILOT-NAME> <application cmps>' % sys.argv[0]))
    print('You can find your token at https://sysdigcloud.com/#/settings/user')
    sys.exit(1)

#input from CLI
#source environment for fetch
if sys.argv[1]=="default":
    logfile.write(str(timestamp())+" INFO using default environment ...442"+"\n")
    sdc_token = "a08256a9-5637-4151-9ec1-12175032e148"
else:
    logfile.write(str(timestamp())+" INFO using custom source environment"+"\n")
    sdc_token = sys.argv[1]
#destination environment
sdc_token_2 = sys.argv[2]
namespace = []
namespace.append(sys.argv[3])
pilot_name = sys.argv[4]
application_cmp = []
i=5


#Check if pilot already exported to production
for filename in os.listdir():
    if( ("SPX LV" in filename) and (pilot_name in filename)):
        print("ERROR pilot already exists")
        logfile.write(str(timestamp())+"ERROR pilot already exists \n")
        sys.exit(1)

while i<len(sys.argv):
    #print(sys.argv[i])
    #print(i)
    application_cmp.append(sys.argv[i])
    i=i+1

#connect to NPRD environment
sdclient = SdMonitorClient(token=sdc_token, sdc_url='https://eu-fr2.monitoring.cloud.ibm.com')
logfile.write(str(timestamp())+" INFO connecting to "+sdclient.url+"\n")

#http request through sysdig GET api
req = sdclient.url+"/api/v3/dashboards/"
#print(sdclient.url + sdclient._default_dashboards_api_endpoint + sdclient.hdrs.to_s())
r=requests.get(req, headers=sdclient.hdrs,verify=sdclient.ssl_verify)

#response will be a json containing an array of dashboards
data_j = r.json()

#print(r.status_code)
print("\n")
if (r.status_code>399):
    print("error during export")
    logfile.write(str(timestamp())+" ERROR error during export"+"\n")
    print(str(timestamp())+" ERROR "+str(r.json))
    logfile.write(str(timestamp())+" ERROR "+str(r.json)+"\n")
    print("\n")
else: 
    print("Export finished succesfully")
    logfile.write(str(timestamp())+" INFO Export finished successfully"+"\n")
    print("\n")

dashboard_ids = []
#conversion in the right dashboard post API version
for dashboard in data_j["dashboards"]:
    dashboard_ids.append(dashboard["name"])
    dashboard_ids.append(dashboard["id"])
    del dashboard["id"]
    del dashboard["version"]

print(dashboard_ids)

print("applying new filters to exported dashboards")
logfile.write(str(timestamp())+" INFO applying new filters to exported dashboards"+"\n")
print("\n")

#Building and applying the right prefiltering    
for dashboard in data_j["dashboards"]:
    if( "SPX LV" in dashboard["name"]):
    #for all our dashboard first i create one insights for each app component
        if("SPX LV3 Application Insights" in dashboard["name"]):
            
            for elem_app in application_cmp:
                act_elem = []
                act_elem.append(elem_app)
                print(dashboard["name"]+" "+namespace[0]+" "+elem_app+".json")    
                #print(dashboard["name"])
                if("scopeExpressionList" in dashboard):
                    for elem in dashboard["scopeExpressionList"]:
                        if(elem["displayName"]=="namespace"):
                            elem["value"]=namespace
                        #print(elem)
                        if(elem["displayName"]=="container"):
                            elem["value"]=act_elem
                        #print(elem)
                        if(elem["displayName"]=="cluster"):
                            empty_arr = []
                            elem["value"]=empty_arr
                
                dashboard["name"] = "SPX LV3 Application Insights"+" "+pilot_name+" "+elem_app
                jsonString = json.dumps(dashboard)
                jsonFile = open(dashboard["name"]+".json", "w")
                jsonFile.write("{\"dashboard\":")
                jsonFile.write(jsonString)
                jsonFile.write("}")
                jsonFile.close()
                logfile.write(str(timestamp())+" INFO "+dashboard["name"]+" "+pilot_name+" "+elem_app+".json created successfully"+"\n")
                print("created successfully")
        
        #now i'm going to create all the remaining dashboard with the prefiltering specified by args
        else:
            print(dashboard["name"])
            if("scopeExpressionList" in dashboard):
                for elem in dashboard["scopeExpressionList"]:
                    if(elem["displayName"]=="namespace"):
                        elem["value"]=namespace
                    #print(elem)
                    if(elem["displayName"]=="container"):
                        elem["value"]=application_cmp
                    #print(elem)
                    if(elem["displayName"]=="cluster"):
                            empty_arr = []
                            elem["value"]=empty_arr
                    
            dashboard["name"] = dashboard["name"]+" "+pilot_name
            jsonString = json.dumps(dashboard)
            jsonFile = open(dashboard["name"]+".json", "w")
            jsonFile.write("{\"dashboard\":")
            jsonFile.write(jsonString)
            jsonFile.write("}")
            jsonFile.close()
            logfile.write(str(timestamp())+" INFO "+dashboard["name"]+" "+pilot_name+".json created successfully"+"\n")
            print("created successfully!!!")

jsonString = json.dumps(data_j)
jsonFile = open("data.json", "w")
jsonFile.write(jsonString)
jsonFile.close()

print("\n")
print("dashboards builded and ready to be imported")
logfile.write(str(timestamp())+" INFO dashboards builded and ready to be imported"+"\n")
print("\n")
print("starting importing dashboard")
logfile.write(str(timestamp())+" INFO starting importing dashboard"+"\n")
print("\n")

#sending to production  
sdclient_2 = SdMonitorClient(token=sdc_token_2, sdc_url='https://eu-fr2.monitoring.cloud.ibm.com')
print("connecting to "+sdclient_2.url)
logfile.write(str(timestamp())+" INFO connecting to "+sdclient_2.url+"\n")
print("\n")
                            
req = sdclient_2.url+"/api/v3/dashboards/"

for filename in os.listdir():
    
    if( ("SPX LV" in filename) and (pilot_name in filename)):
        #print(dashboard["name"]+".json")
        data_2 = json.load(open(filename, 'r'))
        #print(data_2)
        r2=requests.post(req, headers=sdclient_2.hdrs, data=json.dumps(data_2), verify=sdclient_2.ssl_verify)
        #print(r2.status_code) 
        if(r2.status_code>399):
            #print(filename)
            print("error on dashboard => "+filename)
            logfile.write(str(timestamp())+" ERROR status_code="+ str(r2.status_code)+" on dashboard => "+filename+"\n")
            print(r2.json())
            logfile.write(str(timestamp())+" ERROR detail = "+ str(r2.json())+"\n")
        else:
            print(filename+" dashboard created successfully")
            logfile.write(str(timestamp())+" INFO dashboard "+filename+" created successfully"+"\n")
print("\n")

print(" THE END ")
logfile.write(str(timestamp())+" INFO ####################### THE END #######################"+"\n")    

                             