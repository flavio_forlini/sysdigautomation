#!/usr/bin/env python
#
# This example shows how to delete a dashboard
#

import getopt
import sys
import datetime
import os
from sdcclient import SdMonitorClient

# ct stores current time
def timestamp():
    ct = datetime.datetime.now()
    #print("current time:-", ct)
    return ct

#opening log file
logfile = open("spx_dashboard_delete_script.log", "a")
#
# Parse arguments
#
def usage():
    print('usage: <sysdig-token> <Keyword in dashboard to delete>')
    print('You can find your token at https://app.sysdigcloud.com/#/settings/user')
    sys.exit(1)

try:
    opts, args = getopt.getopt(sys.argv[1:], "p:", ["pattern="])
except getopt.GetoptError:
    usage()

pattern = "API Test"
for opt, arg in opts:
    if opt in ("-p", "--pattern"):
        pattern = arg

if len(args) != 2:
    usage()

sdc_token = args[0]
namespace = args[1]

#
# Instantiate the SDC client
#
sdclient = SdMonitorClient(token=sdc_token, sdc_url='https://eu-fr2.monitoring.cloud.ibm.com')

#
# List the dashboards
#
ok, res = sdclient.get_dashboards()
if not ok:
    print(res)
    logfile.write(str(timestamp())+" ERROR CONNECTING SYSDIG"+str(res)+"\n")
    sys.exit(1)
    

#dashboard_array = ["SPX_LVL1_Namespace_Overview","SPX_LVL1_Cluster_Overview","SPX_Node_Overview","SPX_Node_Overview","SPX Container Resource Usage & Troubleshooting PRISMA","test dasboard from file","SPX_LVL1_CPU_Allocation_Optimization","SPX_LVL1_Cluster_Capacity_Planning","SPX_architecture_table","SPX_LVL3_Container_Prisma_Server","SPX DaemonSet Overview","spx Workload Status & Performance","SPX Cluster Capacity Planning","spx Workload Status & Performance","spx MongoDB Troubleshooting","SPX LV2 Kubernates Monitoring","SPX Homepage Cloud Monitoring","SPX_LVL3_Container_Prisma_Client","SPX LV1 Cloud Monitoring Overview","SPX LV2 Database Monitoring"]

#
# Delete all the dashboards containing pattern
#
#for dashboard in res['dashboards']:
#    for name in dashboard_array:
#        if name in dashboard['name']:
#            print(("Deleting " + dashboard['name']))
#            ok, res = sdclient.delete_dashboard(dashboard)
#            if not ok:
#                print(res)
                
for dashboard in res['dashboards']: 
    if ("SPX LV" in dashboard['name']) and ( namespace in dashboard['name']):
        print(("Deleting " + dashboard['name']))
        logfile.write(str(timestamp())+" INFO "+"Deleting " + dashboard['name']+"\n")
        ok, res = sdclient.delete_dashboard(dashboard)
        if not ok:
            print(res)               
            logfile.write(str(timestamp())+" ERROR "+str(res)+"\n")
        else:
            print("dashboard "+dashboard['name']+" deleted")
            logfile.write(str(timestamp())+" INFO "+"dashboard "+dashboard['name']+" deleted"+"\n")
            
for filename in os.listdir():
    if( ("SPX LV" in filename) and (namespace in filename)):
        os.remove(filename)