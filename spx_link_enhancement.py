import sys
import re
import json
import requests
import os
from sdcclient import IbmAuthHelper, SdMonitorClient, SdcClient
from sdcclient._common import _SdcCommon


if(len(sys.argv)<3):
    print("USAGE:")
    print("usare <PILOT-NAME> <sysdig token> in questo ordine")
    sys.exit()
    
#get dashboard for id retrieve 
#reconnect to PRD environment
sdclient = SdMonitorClient(token=sys.argv[2], sdc_url='https://eu-fr2.monitoring.cloud.ibm.com')
#logfile.write(str(timestamp())+" INFO connecting to "+sdclient.url+"\n")

#http request through sysdig GET api
req = sdclient.url+"/api/v3/dashboards/"
#print(sdclient.url + sdclient._default_dashboards_api_endpoint + sdclient.hdrs.to_s())
r=requests.get(req, headers=sdclient.hdrs,verify=sdclient.ssl_verify)

#response will be a json containing an array of dashboards
data_j = r.json()

#print(r.status_code)
print("\n")
if (r.status_code>399):
    print("error during export")
    #logfile.write(str(timestamp())+" ERROR error during import from production"+"\n")
    print(str(timestamp())+" ERROR "+str(r.json))
    #logfile.write(str(timestamp())+" ERROR "+str(r.json)+"\n")
    print("\n")
else: 
    print("import from production finished succesfully")
    #logfile.write(str(timestamp())+" INFO import from production finished successfully"+"\n")
    print("\n")
    
#first i collect lvl2 dashboard id
dashboards_database = []
dashboard_lv1_id = []
dashboards_lvl2_ids = {}
dashboards_lvl3_ids = {}
dashboards_lvl4_ids = {}



for dashboard in data_j["dashboards"]:
    if ("SPX Cluster Database Monitoring" in dashboard["name"]):
        print("found dashboard database")
        dashboard["panels"][0]["markdownSource"]
        dashboards_database.append(dashboard["id"])

for dashboard in data_j["dashboards"]:
    if ("SPX LV1" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :
        dashboard["panels"][0]["markdownSource"]
        dashboard_lv1_id.append(dashboard["id"])

for dashboard in data_j["dashboards"]:
    if ("SPX LV2" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :
        #dashboards_lvl2_ids.append(dashboard["name"])
        #dashboards_lvl2_ids.append(dashboard["id"])
        dashboards_lvl2_ids[dashboard["name"]]=dashboard["id"]
print(dashboards_lvl2_ids)

for dashboard in data_j["dashboards"]:
    if ("SPX LV3" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :
        dashboards_lvl3_ids[dashboard["name"]]=dashboard["id"]
print(dashboards_lvl3_ids)

for dashboard in data_j["dashboards"]:
    if ("SPX LV4" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :
        dashboards_lvl4_ids[dashboard["name"]]=dashboard["id"]
print(dashboards_lvl4_ids)

print("DASHBOARD DATABASE")
print(dashboards_database)

#change links in LV1
for dashboard in data_j["dashboards"]:
    if(sys.argv[1] in dashboard["name"]):
        print("updating dashboard:")
        print(dashboard["name"])
    if ("SPX LV1" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :

      
        dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=Database Monitoring]\(#/dashboards\/)\d+",str(dashboards_database.pop()),str(dashboard["panels"][0]["markdownSource"]))
        for key in dashboards_lvl2_ids:
            if "Kubernates Monitoring" in key:
                #print("YES FIRST KEY")
                dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=Kubernetes Monitoring]\(#/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][0]["markdownSource"]))
                #print(dashboard["panels"][0])
            if "Application - Container" in key:
                #print("YES SECOND KEY")
                dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=Application Monitoring]\(#/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][0]["markdownSource"]))
                #print(dashboard["panels"][0])
            

        #using update api
        ok, res = sdclient.update_dashboard(dashboard)
        #
        # Check for successful update
        #
        if not ok:
            print(res)
            sys.exit(1)
        else:
            print("dashboard: ")
            print(dashboard["name"])
            print("updated successfully")
            print("###########################################################################")
        
        
    if ("SPX LV2" in dashboard["name"]) and (sys.argv[1] in dashboard["name"]):      
        
        if "Kubernates Monitoring" in dashboard["name"]:
            for key in dashboards_lvl3_ids:
                
                if "Kube - Node Status & Performance" in key:
                    #print("YES FIRST KEY")
                    dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=SPX LV3 Kube - Node Status & Performance]\(#/dashboards\/)\d+",str(dashboards_lvl3_ids[key]),str(dashboard["panels"][0]["markdownSource"]))
                    #print(dashboard["panels"][0])
                if "Kube - Workload Status & Performance" in key:
                    #print("YES SECOND KEY")
                    dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=SPX LV3 Kube - Workload Status & Performance]\(#/dashboards\/)\d+",str(dashboards_lvl3_ids[key]),str(dashboard["panels"][0]["markdownSource"]))
                    #print(dashboard["panels"][0])
      
            
            dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=SPX LV1 Cloud Monitoring]\(#\/dashboards\/)\d+",str(dashboard_lv1_id[0]),str(dashboard["panels"][0]["markdownSource"]))
            #print(dashboard["panels"][0])

            #using update api
            ok, res = sdclient.update_dashboard(dashboard)
            #
            # Check for successful update
            #
            if not ok:
                print(res)
                sys.exit(1)
            else:
                print("dashboard: ")
                print(dashboard["name"])
                print("updated successfully")
                print("###########################################################################")
                
    if ("SPX LV2" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :
        #dashboard["panels"][0]["markdownSource"]
        
        if "Database Monitoring" in dashboard["name"]:
            for key in dashboards_lvl3_ids:
                #print("#######KEY#######")
                #print(key)
                if "SPX LV3 MongoDB Overview" in key:
                    #print("YES FIRST KEY")
                    dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=SPX LV3 MongoDB Overview]\(#/dashboards\/)\d+",str(dashboards_lvl3_ids[key]),str(dashboard["panels"][0]["markdownSource"]))
                    #print(dashboard["panels"][0])
                if "SPX LV3 PostgreSQL Overview" in key:
                    #print("YES SECOND KEY")
                    dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=SPX LV3 PostgreSQL Overview]\(#/dashboards\/)\d+",str(dashboards_lvl3_ids[key]),str(dashboard["panels"][0]["markdownSource"]))
                    #print(dashboard["panels"][0])
      
            #print("previous KEY")
            #print(dashboard_lv1_id[0])
            dashboard["panels"][0]["markdownSource"] = re.sub(r"(?<=SPX LV1 Cloud Monitoring]\(#\/dashboards\/)\d+",str(dashboard_lv1_id[0]),str(dashboard["panels"][0]["markdownSource"]))
            #print(dashboard["panels"][0])

            #jsonFile = open("lv2_database.json", "w")
            #jsonFile.write(str(dashboard))
            #jsonFile.close()
            #print(data_j[0])
            #req = sdclient.update_dashboard(dashboard)
            ok, res = sdclient.update_dashboard(dashboard)
            #
            # Check for successful update
            #
            if not ok:
                print(res)
                sys.exit(1) 
            else:
                print("dashboard: ")
                print(dashboard["name"])
                print("updated successfully")
                print("###########################################################################")
 
    if ("SPX LV2" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :
        #dashboard["panels"][0]["markdownSource"]
        
        if "SPX LV2 Application - Container Resource Usage" in dashboard["name"]:
            string_to_insert = 'next level'
            for key in dashboards_lvl3_ids:
                #print("#######KEY##############")
                #print(key)
                if "Application Insights" in key:
                    split_cmp = key.split(" ")
                    string_to_insert = string_to_insert+"\n\n**[SPX LV3 Application Insights %s](#/dashboards/%i)**\n"%(split_cmp[len(split_cmp)-1],dashboards_lvl3_ids[key])
                    
                    #print("YES FIRST KEY")
                    dashboard["panels"][10]["markdownSource"] = re.sub(r"(?<=SPX LV3 Application Insights]\(#\/dashboards\/)\d+",str(dashboards_lvl3_ids[key]),str(dashboard["panels"][10]["markdownSource"]))
                    #print(dashboard["panels"][10])
            print("final string to insert")
            string_to_insert = string_to_insert + "\n\nprevious level\n\n**[SPX LV1 Cloud Monitoring](#/dashboards/%i)**\n"%(dashboard_lv1_id[0])
            print(string_to_insert)
               
      
            #print("previous KEY")
            #print(str(dashboard_lv1_id[0]))
            dashboard["panels"][10]["markdownSource"] = string_to_insert
            #print(dashboard["panels"][10])

            #jsonFile = open("lv2_kubernetes_overview.json", "w")
            #jsonFile.write(str(dashboard))
            #jsonFile.close()
            #print(data_j[0])
            #req = sdclient.update_dashboard(dashboard)
            ok, res = sdclient.update_dashboard(dashboard)
            #
            # Check for successful update
            #
            if not ok:
                print(res)
                sys.exit(1)
            else:
                print("dashboard: ")
                print(dashboard["name"])
                print("updated successfully")
                print("###########################################################################")
               
    if ("SPX LV3" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :
        #dashboard["panels"][0]["markdownSource"]
        
        if "SPX LV3 MongoDB Overview" in dashboard["name"]:
            for key in dashboards_lvl2_ids:
   
                if "SPX LV2 Database Monitoring" in key:

                   for panel in dashboard["panels"]:
                    
                       if("SPX LV3 Database Monitoring" in panel["name"]):
                          
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           #print(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"])
                           #dashboard["panels"][panel[i]]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#\/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][panel[i]]["markdownSource"]))
                           #print(dashboard["panels"][panel[i]]) 
                           
                           #jsonFile = open("lv2_kubernetes_overview.json", "w")
                           #jsonFile.write(str(dashboard))
                           #jsonFile.close()
                           #print(data_j[0])
                           #req = sdclient.update_dashboard(dashboard)
                           ok, res = sdclient.update_dashboard(dashboard)
                           #
                           # Check for successful update
                           #
                           if not ok:
                                print(res)
                                sys.exit(1)
                           else:
                                print("dashboard: ")
                                print(dashboard["name"])
                                print("updated successfully")
                                print("###########################################################################")
                           
            
                       
                       

                    
         
        if "SPX LV3 PostgreSQL Overview" in dashboard["name"]:
            
            for key in dashboards_lvl2_ids:
     
                if "SPX LV2 Database Monitoring" in key:
                  
                    for panel in dashboard["panels"]:
                       if("SPX LV3 Database Monitoring" in panel["name"]):
                           #print(panel)
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           #dashboard["panels"][panel[i]]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#\/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][panel[i]]["markdownSource"]))
                           #print(dashboard["panels"][panel[i]])
                           ok, res = sdclient.update_dashboard(dashboard)
                           #
                           # Check for successful update
                           #
                           if not ok:
                                print(res)
                                sys.exit(1)
                           else:
                                print("dashboard: ")
                                print(dashboard["name"])
                                print("updated successfully")
                                print("###########################################################################")
                           
                         
         
        if "SPX LV3 Kube - Node Status & Performance" in dashboard["name"]:
            
            for key in dashboards_lvl2_ids:
  
                if "SPX LV2 Kubernates Monitoring" in key:
          
                   for panel in dashboard["panels"]:
              
                       if("SPX LV3 Kubernates Monitoring" in panel["name"]):
                           #print(panel)
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV2 Kubernates Monitoring]\(#/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV4 Kube - Pod Status & Performance]\(#/dashboards\/)\d+",str(dashboards_lvl4_ids["SPX LV4 Kube - Pod Status & Performance "+sys.argv[1]]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           #print(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"])
                           #dashboard["panels"][panel[i]]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#\/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][panel[i]]["markdownSource"]))
                           #print(dashboard["panels"][panel[i]]) 
                           
                           #jsonFile = open("lv2_kubernetes_overview.json", "w")
                           #jsonFile.write(str(dashboard))
                           #jsonFile.close()
                           #print(data_j[0])
                           #req = sdclient.update_dashboard(dashboard)
                           ok, res = sdclient.update_dashboard(dashboard)
                           #
                           # Check for successful update
                           #
                           if not ok:
                                print(res)
                                sys.exit(1)
                           else:
                                print("dashboard: ")
                                print(dashboard["name"])
                                print("updated successfully")
                                print("###########################################################################")
                           
                         
                       
        if "SPX LV3 Kube - Workload Status & Performance" in dashboard["name"]:
            
            for key in dashboards_lvl2_ids:
         
                if "SPX LV2 Kubernates Monitoring" in key:
      
                   for panel in dashboard["panels"]:
                 
                       if("SPX LV4 Kubernates Monitoring" in panel["name"]):
                           #print(panel)
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV2 Kubernates Monitoring]\(#/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV4 Kube - Job Overview]\(#/dashboards\/)\d+",str(dashboards_lvl4_ids["SPX LV4 Kube - Job Overview "+sys.argv[1]]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           #print(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"])
                           #dashboard["panels"][panel[i]]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#\/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][panel[i]]["markdownSource"]))
                           #print(dashboard["panels"][panel[i]]) 
                           
                           
                           #req = sdclient.update_dashboard(dashboard)
                           ok, res = sdclient.update_dashboard(dashboard)
                           #
                           # Check for successful update
                           #
                           if not ok:
                                print(res)
                                sys.exit(1) 
                           else:
                                print("dashboard: ")
                                print(dashboard["name"])
                                print("updated successfully")
                                print("###########################################################################")
                    
                       
        if "SPX LV3 Application Insights" in dashboard["name"]:
            
            for key in dashboards_lvl2_ids:
              
                if "SPX LV2 Application - Container Resource Usage & Troubleshooting" in key:
                  
                   for panel in dashboard["panels"]:
                 
                       if("New Panel" in panel["name"]):
                           #print(panel)
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV2 Application Monitoring]\(#/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           #print(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"])
                           #dashboard["panels"][panel[i]]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#\/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][panel[i]]["markdownSource"]))
                           #print(dashboard["panels"][panel[i]]) 
                           
                          
                           
                           #req = sdclient.update_dashboard(dashboard)
                           ok, res = sdclient.update_dashboard(dashboard)
                           #
                           # Check for successful update
                           #
                           if not ok:
                                print(res)
                                sys.exit(1)
                           else:
                                print("dashboard: ")
                                print(dashboard["name"])
                                print("updated successfully")
                                print("###########################################################################")
                  
        
    if ("SPX LV4" in dashboard["name"]) and (sys.argv[1] in dashboard["name"])  :
        
        if "SPX LV4 Kube - Job Overview" in dashboard["name"]:
            for key in dashboards_lvl3_ids:
                
                if "SPX LV3 Kube - Workload Status & Performance" in key:
                   
                   for panel in dashboard["panels"]:
                       
                       if("SPX LV4 Kubernates Monitoring" in panel["name"]):
                           #print(panel)
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV3 Kube - Workload Status & Performance]\(#/dashboards\/)\d+",str(dashboards_lvl3_ids[key]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           #print(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"])
                           #dashboard["panels"][panel[i]]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#\/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][panel[i]]["markdownSource"]))
                           #print(dashboard["panels"][panel[i]]) 
                           
                           #jsonFile = open("lv2_kubernetes_overview.json", "w")
                           #jsonFile.write(str(dashboard))
                           #jsonFile.close()
                           #print(data_j[0])
                           #req = sdclient.update_dashboard(dashboard)
                           ok, res = sdclient.update_dashboard(dashboard)
                           #
                           # Check for successful update
                           #
                           if not ok:
                                print(res)
                                sys.exit(1)
                           else:
                                print("dashboard: ")
                                print(dashboard["name"])
                                print("updated successfully")
                                print("###########################################################################")
                           
                      
                       
        if "SPX LV4 Kube - Pod" in dashboard["name"]:
            
            for key in dashboards_lvl3_ids:
                
                if "SPX LV3 Kube - Node Status & Performance" in key:
                   
                   for panel in dashboard["panels"]:
                       
                       if("SPX LV4 Kubernates Monitoring" in panel["name"]):
                           #print(panel)
                           dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"] = re.sub(r"(?<=SPX LV3 Kube - Node Status & Performance]\(#/dashboards\/)\d+",str(dashboards_lvl3_ids[key]),str(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"]))
                           #print(dashboard["panels"][dashboard["panels"].index(panel)]["markdownSource"])
                           #dashboard["panels"][panel[i]]["markdownSource"] = re.sub(r"(?<=SPX LV2 Database Monitoring]\(#\/dashboards\/)\d+",str(dashboards_lvl2_ids[key]),str(dashboard["panels"][panel[i]]["markdownSource"]))
                           #print(dashboard["panels"][panel[i]]) 
                           
                           #jsonFile = open("lv2_kubernetes_overview.json", "w")
                           #jsonFile.write(str(dashboard))
                           #jsonFile.close()
                           #print(data_j[0])
                           #req = sdclient.update_dashboard(dashboard)
                           ok, res = sdclient.update_dashboard(dashboard)
                           #
                           # Check for successful update
                           #
                           if not ok:
                                print(res)
                                sys.exit(1)
                           else:
                                print("dashboard: ")
                                print(dashboard["name"])
                                print("updated successfully")
                                print("###########################################################################")
                           
                                               
print(" THE END ")
#logfile.write(str(timestamp())+" INFO ####################### THE END #######################"+"\n")    